# hello-world-yarn-plugin
### its a project built to Test how yarn plugins and hooks works which is strictly for yarn2

* In debian we use yarnpkg instead of yarn as yarn command is already taken by cmdtest package
```
git clone <repo link>
yarnpkg init
yarnpkg hello
```
`You get a welcome message`

* for installing of package there is a dynamic message that gets printed out
```
yarnpkg install <package_name>
```
e.g
```
yarnpkg install express
```
`you get an awesome message and total number of dependencies`

* To learn more about yarnpkg go to: https://yarnpkg.com/features/plugins

