module.exports = {
  name: `plugin-hello-world`,
  factory: require => {
    const {Command} = require(`clipanion`);

    class HelloWorldCommand extends Command {
      async execute() {
        this.context.stdout.write(`This is my very first  plugin 😎\n`);
      }
    }

    HelloWorldCommand.addPath(`hello`);

    HelloWorldCommand.usage = Command.Usage({
      description: `hello world!`,
      details: `
        This command will print a nice message.
      `,
      examples: [[
        `Say hello to debian  user`,
      ]],
    });

    return {
      commands: [
        HelloWorldCommand,
      ],
     hooks: {
          afterAllInstalled(project) {
	  console.log(`There are ${project.storedChecksums.size} dependencies now\n`)
            console.log(`Package installed successfully!`);
          }
        } 
    };
  }
};
